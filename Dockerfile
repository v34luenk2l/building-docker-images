FROM python:3.9-slim-bullseye
RUN pip install cowsay
ENTRYPOINT python -c "import cowsay; cowsay.tux('hello')"
